"""
Bayesian Statistics Calculator
------------------------------
Caclulates bayesian statistical
problems. Which use prior knowledge
along side tests.
------------------------------
Freq = Certainty of test
Prior = Prior Knowledge
Tests = Number of tests
"""

def bayes(freq, prior, tests):
    freq, prior = freq / 100, prior / 100
    if tests == 0:
        return (freq * prior) / ((freq * prior) + ((1 - freq) * (1 - prior)))
    return (freq * bayes(freq * 100, prior * 100, tests - 1)) / ((freq * bayes(freq * 100, prior * 100, tests - 1)) + ((1 - freq) * (1 - bayes(freq * 100, prior * 100, tests - 1))))

print(bayes(25, 8, 10))
