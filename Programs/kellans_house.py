from turtle import *   
space = Screen()          
ancalagon_of_the_ancient_darkness = Turtle()   

def teleport(x, y):
  ancalagon_of_the_ancient_darkness.speed(0)
  ancalagon_of_the_ancient_darkness.penup()
  ancalagon_of_the_ancient_darkness.goto(x, y)
  ancalagon_of_the_ancient_darkness.pendown()
  ancalagon_of_the_ancient_darkness.speed(3)

def travel(sc, sd, dr, ds): # This is for a destination you know relative to a object but not by it's coordinates
  ancalagon_of_the_ancient_darkness.penup()
  ancalagon_of_the_ancient_darkness.goto(sc, sd)
  ancalagon_of_the_ancient_darkness.setheading(dr)
  ancalagon_of_the_ancient_darkness.forward(ds)
  ancalagon_of_the_ancient_darkness.pendown()
  
  
def conj_rectangle(h, w, c, d):
  teleport(c, d)
  ancalagon_of_the_ancient_darkness.setheading(90)
  for x in range(2):
    ancalagon_of_the_ancient_darkness.forward(h)
    ancalagon_of_the_ancient_darkness.right(90)
    ancalagon_of_the_ancient_darkness.forward(w)
    ancalagon_of_the_ancient_darkness.right(90)
  
def conj_isostriangle(h, w, v, c, d):
  side = ((h ** 2) + ((w * 0.5) ** 2)) ** 0.5
  sh = 180 - (90 + (0.5 * v))
  teleport(c, d)
  ancalagon_of_the_ancient_darkness.setheading(sh)
  ancalagon_of_the_ancient_darkness.forward(side)
  ancalagon_of_the_ancient_darkness.right(v)
  ancalagon_of_the_ancient_darkness.forward(side)
  ancalagon_of_the_ancient_darkness.goto(c, d)
  
          
conj_rectangle(100, 150, -75, -100) # Wall
conj_isostriangle(75, 150, 90, -75, 0) # Roof
conj_rectangle(55, 34, -17, -100) # Door
conj_rectangle(18, 18, 25, -63) # Window

# Chimney
travel(-75, 0, 45, 43.75)
ancalagon_of_the_ancient_darkness.color("red")
ancalagon_of_the_ancient_darkness.setheading(90)
ancalagon_of_the_ancient_darkness.forward(40)
for x in range(2):
  ancalagon_of_the_ancient_darkness.right(90)
  ancalagon_of_the_ancient_darkness.forward(20)
ancalagon_of_the_ancient_darkness.color("black")
# End Chimney

conj_rectangle(20, 24, -12, -70) # Door Window

# Door Window Muntin
teleport(-12, -60)
ancalagon_of_the_ancient_darkness.setheading(0)
ancalagon_of_the_ancient_darkness.forward(24)
teleport(0, -70)
ancalagon_of_the_ancient_darkness.setheading(90)
ancalagon_of_the_ancient_darkness.forward(20)
# End Door Window Muntin

ancalagon_of_the_ancient_darkness.hideturtle()
input()

#After making and applying the functions I cut 20 lines of code from 72 -> 52.        I themed the new program after fantasy. My turtle was named after ancalagon the black and my functions were supposed to be spells; conjure [shape], teleport, travel, etc.  I wanted the house to have a more real look so I gave it more normal dimensions for   the roof and wall. That's all, good day.
