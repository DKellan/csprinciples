from PIL import Image


def write_on_big_canvas(img, canvas, start, rule):
    pixels = img.load()
    w = img.size[0]
    h = img.size[1]

    canvas_pixels = canvas.load()

    for col in range(w):
        for row in range(h):
            r, g, b = pixels[col, row]
            canvas_x = start[0] + col
            canvas_y = start[1] + row
            if canvas_x < canvas.width and canvas_y < canvas.height:
                canvas_pixels[canvas_x, canvas_y] = rule(r, g, b)


def do_nothing(r, g, b):
    return (r, g, b)


def only_red(r, g, b):
    return (r, 0, 0)


def only_green(r, g, b):
    return (0, g, 0)


def only_blue(r, g, b):
    return (0, 0, b)


def black_and_white(r, g, b):
    avg = (r + g + b) // 3
    return (avg, avg, avg)


def negative(r, g, b):
    return (256-r, 256-g, 256-b)


# Create an image from a file
img = Image.open("jelkner_dc.jpg")

# Create an image 3 times the width and height of the original
canvas = Image.new(mode="RGB", size=(3 * img.width, 2 * img.height))

write_on_big_canvas(img, canvas, (0, 0), do_nothing)
write_on_big_canvas(img, canvas, (img.width, 0), only_red)
write_on_big_canvas(img, canvas, (2*img.width, 0), only_green)
write_on_big_canvas(img, canvas, (0, img.height), only_blue)
write_on_big_canvas(img, canvas, (img.width, img.height), black_and_white)
write_on_big_canvas(img, canvas, (2*img.width, img.height), negative)

canvas.show()
