from random import randint

correct = 0
print("")
mst = int(input("Easy Mode (1) | Master Mode (2)\n")

print("Math quiz")
print("\n")
level = int(input("Select Difficulty…\n5 Levels\n"))
print("")
qnum = int(input("How many questions?\n"))


for q in range(qnum):
    or101 = randint(1, (10 * mst))
    or202 = randint(1, (10 * mst))
    od = randint(1, level)
    if od == 1:
        or101 = randint(1, (100 * mst))
        or202 = randint(1, (100 * mst))
        od = "plus"
        answer = or101 + or202
    elif od == 2:
        x = randint(10, (90 * mst))
        or101 = randint(x, (100 * mst))
        or202 = randint(1, x)
        od = "minus"
        answer = or101 - or202
    elif od == 3:
        od = "times"
        answer = or101 * or202
    elif od == 4:
        or202 = randint(1, (6 * mst))
        or101 = or202*(randint(1, (9 + (6 ** (mst - 1)))))
        od = "Divided"
        answer = or101 / or202
    else:
        or101 = randint(1, (3 + mst))
        or202 = randint(1, (3 + mst))
        od = "to the power of"
        answer = or101 ** or202

    question = f"What is {or101} {od} {or202}?"

    a1 = int(input(f"{question} \n Answer: "))

    print("")
    if answer == a1:
        print("That's right. Good job. :)")
        correct +=1
    else:
        print("I'm afraid that's not right. You could do better. Try again.")
    print("")

if correct >= (0.9 * qnum):
    print(f"I asked you {qnum} questions. You got {correct} of them right, well done.")
elif correct > (0.5 * qnum) and correct < (0.9 * qnum):
    print(f"I asked you 10 questions. You got {correct} of them right, you passed but you didn't do great.")
elif correct <= (0.5 * qnum) and correct > 0:
    print(f"I asked you 10 questions. You got {correct} of them right, you failed. I'm very angry with you.")
else:
    print("You've done so unbelievably bad. Watch behind your back from now on.")
