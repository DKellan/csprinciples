from gasp import *          # So that you can draw things
from random import randint
from time import sleep

class Player:
    pass

class Robot:
    pass

def make_gridlines():
    for x in range(1, 56):
        Line((x * 10, 0), (x * 10, 550), color=color.BLACK, thickness=1)
        Line((0, x * 10), (550, x * 10), color=color.BLACK, thickness=1)

def place_player():
    global player
    player = Player()
    player.x = random.randint(0, 55)
    player.y = random.randint(0, 55)
    while collided():
        player.x = random.randint(0, 55)
        player.y = random.randint(0, 55)
    player.shape = Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)

def place_robots():
    global robots
    robot = Robot()
    robot.x = random.randint(0, 55)
    robot.y = random.randint(0, 55)
    robot.shape = Box((10 * robot.x, 10 * robot.y), 10, 10, filled=False, color=color.BLACK, thickness=3)
    
    robot2 = Robot()
    robot2.x = random.randint(0, 55)
    robot2.y = random.randint(0, 55)
    robot2.shape = Box((10 * robot.x, 10 * robot.y), 10, 10, filled=False, color=color.BLACK, thickness=3)

    robot3 = Robot()
    robot3.x = random.randint(0, 55)
    robot3.y = random.randint(0, 55)

def move_player():
    global player
    desired = update_when('key_pressed')
    while desired == "space":
        remove_from_screen(player.shape)
        place_player()
        desired = update_when('key_pressed')
    if desired == "h" and player.y < 55: #forward
        player.y += 1
    elif desired == "j" and player.y > 0: #backward
        player.y -= 1
    elif desired == "k" and player.x > 0: #left
        player.x -= 1
    elif desired == "l" and player.x < 55: #right
        player.x += 1
elif desired == "n": #swap coords
        player.x, player.y = player.y, player.x
    elif desired == "b": #double backwards 
        if player.y > 1:
            player.y -= 2
        else:
            player.y -= 1
    elif desired == "u": #double forwards 
        if player.y < 54: 
            player.y += 2
        else:
            player.y += 1
    move_to(player.shape, (10 * player.x + 5, 10 * player.y + 5))

def move_robot():
    global robots
    if robot.y < player.y:
        robot.y += 1
        if robot.x < player.x:
            robot.x += 1
        if robot.x > player.x:
            robot.x -= 1
    elif robot.y > player.y:
        robot.y -= 1
        if robot.x < player.x:
            robot.x += 1
        if robot.x > player.x:
            robot.x -= 1
    else:
        if robot.x < player.x:
            robot.x += 1
        if robot.x > player.x:
            robot.x -= 1
    move_to(robot.shape, (10 * robot.x, 10 * robot.y))

def collided():
    if robot.y == player.y and robot.x == player.x:
        Text("Game Over", (275, 275), size=50)
        time.sleep(3)
        return True
    return False


begin_graphics(550, 550)            # Create a graphics window
finished = False

place_robots()
place_player()
make_gridlines()
while not collided():
    move_player()
    move_robot()
    collided()
end_graphics()              # Finished!
