# AP Exam Presentations

* Study Seperate Subjects Seperately
* Save questions you don't get right seperately to practice them even more
* Do practice tests
* Use resources like Khan Academy and Test Guides for the multiple choice section.
* Use the AP website for practice CPT questions.

## Khan Academy Sample Questions

1. Consider this sequence - 101001011000000100110110 - how many bytes is this?
* A) 2 bytes
* B) 3 bytes
* C) 4 bytes
* D) 1 byte
`Answer: 3 bytes`

2. How many bits are in 4 bytes?
* A) 16
* B) 4
* C) 32
* D) 64
`Answer: 32 bits`

3. Rewrite this with a space between each byte - 1011011001101010 .
* A) 1011 0110 0110 1010
* B) 10 11 01 10 01 10 10 10
* C) 1011011001101010
* D) 10110110 01101010
`10110110 01101010`

