## Study Plan
* Review Khan Academy practice problems
* Do practice multiple choice questions
* Review all the big ideas
* Review the Cram Sheet right before the test
* Review the study guide multiple times before the test and on the night before

## Resources:

### Khan Academy Links
* [Lessons](https://www.khanacademy.org/computing/ap-computer-science-principles)
* [APCSP Exam Prep](https://www.khanacademy.org/computing/ap-computer-science-principles/ap-csp-exam-preparation)

### Other Resources
* [Study Guide] (https://uploads-ssl.webflow.com/605fe570e5454a357d1e1811/60a17b3ca029da702d2bf511_SS-APCSP-1.pdf)
* [ChatGPT] (https://chat.openai.com)
* [PDFs with info on each big idea](https://longbaonguyen.github.io/courses/apcsp/apprinciples.html) 
* [APCSP Exam Reference Sheet](https://apcentral.collegeboard.org/media/pdf/ap-computer-science-principles-exam-reference-sheet.pdf) 
* [Practice multiple choice questions](https://library.fiveable.me/ap-comp-sci-p/unit-1/multiple-choice-questions/study-guide/FoS4yNB5OfVommdoARa1) 
* Cram Sheet 
![Cram Sheet](apcspcramsheet.png)


## How to effectively use the resources:
* Don’t wait until the last minute to study.
* Spend extra time reviewing big ideas you didn’t do that well on the practice test. 
* Use Khan Academy for practice problems.
* Read/watch videos to review each big idea.
* Look at the APCSP Exam Reference Sheet.
* Ask questions if you have any! You can use ChatGPT or come during archers period. 
* Print out the cram sheet and put it somewhere in your room that you can easily see and read. (For example, put it on your door so you can read it whenever you walk by)
* If you forgot to study and need to study last-minute, use the cram sheet. It has summarized information from each big idea and tips for the test. 

