#CPT Project Backward Design Plan

**Overall Program Purpose**
The program runs like a classic RPG game, but text-based. The user will start the program and be given a series of opponents to fight. At varying intervals the user will be able to do various other things, such as buying items, or other such things.

**Program Input/Output**
The user will input the characters actions (i.e., attack, heal, etc.). The computer will then effect the elements of the game (i.e., the characters inventory, opponent's health, his own, etc.). These will culminate in the player either losing or winning.

The actual ouput the player sees will be the player and opponent's health, and max health, the final 'you lose' or 'you win' at the end, and some text prompts.

**List Uses**
Every element in the game is represented as either a list or tuple. For a player or monster the list is of their stats and inventory. This makes it easier than writing seperate variables for every attribute of every object in the whole game. 

Dictionaries and objects are two alternative options. Dictionaries are nice because they can be called as player[health] instead of player[1] but are less convenient to set up. On the other hand objects are a superior solution that I am considering strongly.

**Function Specifics**
The program relies heavily on functions for readability and abstractions. Functions exist for attacking, healing, and entering combat. These functions are further seperated into modules and improve the readability and flow understanding of the main file.

**Function Calling**
Since the functions are essentially actions in the game they can and will be used between any two in game entities. Thus, the attack function can work as attack(player, monster) and as attack(monster, player) and for moves to be exchanged between the player and monster, both calls must be used.

This also results in different outputs (the player losing health v.s. the monster losing health).

*~Backward Design Plan Closed~*  
