# CPT Scoring Guidelines

**Scoring:** The scoring is based on six different criteria. Each being worth one point on scored based on it's own rules.

**Row 1: Program Purpose and Function** Shows input, output, clearly explains program functionality. Must not differ from the actual function of the program. Should include a video and written section detailing the overall function of the program.

**Row 2: Data Abstraction** Includes 2 program code statements ONE: that shows the data has been stored in a list TWO: Showing that the data is the same list that is fulfilling the codes purpose.

**Row 3: Managing Complexity** Includes a segment that manages complexity in the program. Explains how the list is used for that purpose. Explains how you could've done the same without it. The list should not be irrelevent to the program. It must be nescessary.

**Row 4: Procedural Abstraction** The program should use a homemade procedure and clearly explain what it does, its purposes, and why it was placed where it was.

**Row 5: Algorithm Implementation**: The written response must detail a procedure that is called multiple times (to prove the nescessity of the procedure), each time the procedure is called it should give a different output. The procedure should also test for conditions (e.g., true or false), and said test should be identified in the code.

**Row 6: Testing** The procedure that is called twice, for two different values, should run a different line of code depending on the result of the afforementioned test.
