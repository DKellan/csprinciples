# Networking Chapter One
The connections between things

## Telephone Connections
Early computers and telephones were limited to the use of these single-transmission at a time, back and forth wires.

The callers contact the operator who manually moves the wires from that person to connect them to their desired other person. This allows the telephone companies to not have to create a different wire for every group of two people. 

```That would require n(n-1)/2 wires for n people, for 5 people that's just 10, but for 20 it's already 140, and at 100 it's 4,950```


```mermaid
graph LR
A[Caller]
B[Caller]
C((Operator))
D(Recipient)
E(Recipient)
A --> C
B --> C
C --> D
C --> E
```
That becomes a problem when you run out of wires:
```mermaid
graph LR
A[Caller]
B[Caller]
C((Operator))
D(Recipient)
E(Recipient)
A --> C
B --> C
C --> D
C --> E
F[Caller] -- XXX --> C
C-- XXX-->G
```

## Proto-networks
The most obvious solution to the problem is by getting a bunch of computers, having them all agree to send information from computer to computer in the background.

```mermaid
graph LR
A[Sender]-->C(Computer A)-->D(Computer B)
C-->E(Computer C)
A-->D
E-->B[Recipient]
```
The problem, of course, arises that if two computers in the network wish to send a message and they both intersect one computer, they will have to wait for the other to finish for their message to send.

If, for example, B wished to send a message to C, and Sender looked to message Recipient, and computer B sent first. You would see Sender's message backlogged and waiting for computer B's to finish sending before it could send its to C and then Recipient.

## Packets

Pretend that Computer B sent a large message, while Sender's was small. By breaking up Computer B's message into many small packets, you no longer have to wait for the entirety of CB's message to send. You can just slip Sender's message in between two packets of CB's and call it done. Hence packets.

```
Keeping up with the demand for better networks, the in
between computers were eventually replaced with 
specialized routers, that did nothing besides connect.
```
