# Chapter 4: Interwork Layer

The layer responsible for routing you from point a to point b

## IP Addresses

Because link adresses don't tell us anything about where the computer is conencted and are kinda just statically connected to each device they are an insufficient tool for mapping layers. It is made up of a bunch of numbers with dots in between them, those represent the places you need to go to get to that computer. Was short now long. By categorizing computers into these numbered folders, routes need only care about one number at a time, just get to number 1, then figure out 2, etc. 

## Routing

The computer needs to know what path to get to things, it doesn't know off the bat so it asks its local routers. As your computer progresses with this proccess it pretty much maps out everything it'll need. If it breaks a link it'll restart and rediscover everything, without the broken link. Routers are constantly busy updating their maps in spare time.

Your packets slowly approach their destination and get closer to being discarded with each step. If they waste to much time they are anhilated. Your computer also has a feature called traceroute that guesses where your packets will go.

## Question Problem
 65,536 is 256^2, and seemingly there are four numbers between the dots and after 212.78 you have two left over each going from 0 to 255 and 256 ^2 is 65,536.
