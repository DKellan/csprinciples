# Networking Chapter Two

## Layers
There are four layers,

 1. Link
 2. Transport
 3. Interwork
 4. Application

Each one building on the last.

## Link
The most basic connection between the computers, raw and unfettered.

```
Data Collision: Two bits of data being sent at the same time and crashing into one another
```

```
CSMA/CD: A method to circumvent data collision, uses a few steps
1. The computer only sends if no other computer is sending
2. The computer checks to see if other computers are sending in its direction
3a. If the computer's data collides with other data, both sending computers will stop their sending and wait, after a certain amount of time (different for both computers), they will both resend
4. The computer will stop after each packet to give other computers some time to start their sends
```

## Interwork

Another layer of abstraction on the 'link' layer. Which is sending from router to router. The essence of this layer is the routers deciding what path to send your data around.

It's really simple, calculating the fastest path between all the routers every time would be a nightmare, so instead the router uses an educated guess to send you roughly closer, and you eventually make some sort of stochastic descent towards your target.

## Transport

Pretty simple, basically checks the metadata of each packet so they can be re-organized even if they get sent out of order. If one packet gets lost or really delayed because of bad interworking then the recipient computer can just ask for a new one. No problem.

## Application

What you're doing with the stuff above it. Email and messaging are early examples. The world wide web is the most common.
```
Server: The backside of the application, that you are connected to from your client.
```
```
Client: On your side, and how you interact with the server, and therefore application.
```

