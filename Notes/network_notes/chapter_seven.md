# Chapter 7- The Application Layer
Here's where we interact with the whole stack in ernest. This layer talks to the network and spits back at us just what we need to see.

## Client / Server

The model by which we make the application layer work is called client/server. Basically, all the internet stuff is the server, and then our device and the protocols we use to grab what's nescessary from the server and show it to the user is the client. I already said all this last time, but the client finds the domain name, calls the assosciated IP, and that is connected to a server with all the data and stuff inside.

## Telephones or Protocol

If you think of the applications as two things talking over a link-internetwork-transport telephone, than you can see an analogy for "rules of conversation" that governs the way the applications must talk, ie, the `protocol`

The protocol we use today for client/server interactions is HTTP or HTTPS, and that can be seen on the URL: `https://`, https - colon - slashtag. HTTP gained its fame because it's pretty concise as far as protocols go, with only 305 pages of wording.

## Telnet

* Follow the protocol and you'll be good, otherwise the server gets angry. 
* Telnet allows you to talk directly with the servers
Use this comman `telnet <domain or IP> <port>`
Once you've pleased the server it'll send you everything you want in a nice and neat package (with headers).

## Other Stuff

### Other Protocols

There's also IMAP for reading mail, and plenty of others, sadly IMAP is to beefy for telnet and you to handle manually (L). 

### Writing Protocol Connections

Super easy in most languages. Remember to specifiy your domain/IP and port, just like before.
