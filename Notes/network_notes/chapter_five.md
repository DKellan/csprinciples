# Chapter 5- The Domain Name System
The Domain Name System (DNS) allows users to access websites by their domain name instead of their IP address. When a computer makes a connection to a system using a domain name address, it first looks up the IP address that corresponds to the domain name. This step also makes it easier to move a server from one location to another as the server is given a new IP address and the DNS entry for the domain address is updated. This allows end users to access the server without being affected by the server's location change.

## Allocating Domain Names
IP addresses are allocated based on where a new network connects to the internet while domain names are allocated based on the organizations that own them. ICANN manages the top-level domains like .com, .edu, and .org and assigns them to other organizations to manage. ICANN also assigns two-letter country code top-level domain names like .us, .za, .nl, and .jp to countries around the world, and countries often add second-level TLDs.

Whenever a company is given control over a domain they are allowed to create as many subdomains underneath it as they wish.

```mermaid
graph LR
A[ICANN] --> B[.com]
A[ICAAN] --> D[.org]
A[ICAAN] --> E[.uk] --> C[.gov]
E --> F[.ac]
```
The names are then written like this `****.ac.uk` or `****.ms.gov` with generality going outward, as opposed to IP addresses.
```
It's also worth noting that only the United States is allowed to use .gov as there top level domain (which they generally use instead of a country code like other countries), other countries can use .gov but only as a second level domain they created for their ccTLD.
```
