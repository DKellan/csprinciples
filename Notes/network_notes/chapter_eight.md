# Chapter 8- The Application Layer

## Back in Ye Olden Days
Back in the day the internet was very small scale, and as a result there was a communal trust that nobody would try to steal data. As such the internet protocols were built around speed and efficiency, not around security. Sadly, as the internet grew larger, and in turn more important, it became a greater risk to leave things unsecure. Finally, with the advent of wiFi security became a necessity, since having these great big open wiFi networks was very insecure.

## Solutions
It is virtually impossible to guard all of the world's bajillion routers and servers so that they cannot be scoured for data. Therefore, the solution to the security problem is to encrypt the data before it is sent across the network, and have it decrypted after.
```
Cæser ciphers, an early form of encryption, involved shifting letters down the alphabet. Unfortunately, that strategy is not so effective today.
```

## Keys and Passwords
The great solution to the new problem– how do you decrypt the encryption– involves the obvious. Both parties will exchange a key beforehand, ie, a thing that tells them how to decode messages from the other, and can then utilize this whenever they need it.

The new problem that arises is how do you stop the hacker from just stealing the key first, and then using it to decrypt the document.

## The Solution... 2! Asymmetric Keys
The solution to the new problem is quite ingenious. Basically, it follows the steps below:–
```
1. The server creates its own personal encryption and decryption method, where the encryption method cannot be reversed to find the decryption.
2. The server sends only the encryption key back, so the other can encrypt.
3. The client encodes the doc and sends it.
4. The server decrypts it, and you're good to go.
```
```
Note: The encryption key is also called the public key – and the decryption the private key
```

## Implementation
As a measure not to break anything as they try to change it, the implementation of security took the form as an extra optional layer. SSL. As it turns out, SSL is a pretty easy thing to place on the transport layer, just some extra metadata to specify what the key is.

## Other Stuff
### Checking
You can clearly see whether a site is secure based on its URL, `http://` for not secure, and `https://` for secure. Additionally, there will be a lock symbol on the URL of secure website on most browsers.

### History
It was formerly rare to waste the extra time on security but now-a-days it's pretty common for websites to be secure.

### Certificate Authority
Basically confirms that your key is coming from a trustworthy place.
