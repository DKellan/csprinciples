# Chapter 3: Link Layer

Layer that's responsible for moving something one computer to another.

## Gateway
When your laptop or phone is using WiFi to connect to the Internet, it is sending and receiving data with a low-powered radio. That radio can only transmit for 300 meters, it must forward the packets to your home router, known as the gateway.

## Security Concerns

When a computer sends out a packet every single computer in that radius also gets the packets. Therefore, your computer requires a method of determining which packets it should get and which are sent to other computers and should be ignored.

Becuase of this effect if you are connected to the same gateway as another computer you may be able to steal or incept their data. That's why you should put a password on your wifi.

## Addresses

Every computer has an address and when your computer wishes to connect to a new wifi network it must send out a form asking all devices on the router if they're the head honcho of the network.

If the form is answered you get the address and all is good, if it is not then you are simply not connected to the wifi and you may get the little cross over the radio-waves symbol, or the no-wifi symbol.

## Ordering

To prevent sending packets at the same time every computer has a "carrier sense" that basically tells it if someone else is sending a packet and if it has room to quickly send some stuff. However, if two packets are sent simultaneously then all the data is corrupted and everything is lost.

To prevent situations like this your computer constantly listens back to everything it says and makes sure it's the same as what it meant to send. If it notices corruption both computers wait different random amounts of time before resending.

If their are way to many people sending to the router at the same time it just specificies turns and moves the turn "token" around as it needs.

## Question Problem

Hexadecimal:- 0f:2a:b3:1f:b3:1a
Binary:- 00001111:00101010:10110011:00011111:10110011:00011010
