# Chapter 6- The Transport Layer

## Packet Headers

A packet going across links between its source and destination computers contains a link header, an IP header, a Transport Control Protocol (TCP) header, and the actual data. The link header changes as the packet moves across different links. The IP header holds the source and destination IP addresses and the Time to Live (TTL) for the packet, and it remains unchanged throughout the journey except for the TTL. The TCP headers indicate where the data in each packet belongs, and the source computer keeps track of the position of each packet relative to the beginning of the message or file and places the offset in each packet created and sent.

## Organizing Packets
The transport layer handles packets that arrive out of order and avoids overwhelming the network. The transport layer sends a certain amount of data before waiting for an acknowledgment from the destination computer. The sending computer adjusts the window size based on the speed of acknowledgments from the destination computer. If a packet is lost, the destination computer sends a message to the sending computer indicating where it last received data, and the sending computer resends the data from that position. The combination of these methods creates a simple and reliable way to send large messages or files across a network. The transport layer continuously monitors how quickly it receives acknowledgments and adjusts the window size to send data rapidly or slowly based on the connection speed.

```
Obviously a computer has to hold onto its packets until the other computer gets all of them so they can make sure everything is squared away.
```
## Ports
To connect to a specific server application on a remote computer, a client application needs to know the correct port number. Ports are like telephone extensions that allow multiple server applications to run on the same IP address. When a server application starts up, it listens for incoming connections on a specified port. There are well-known default ports for various server applications, but sometimes servers make applications available at non-standard ports. The port number is indicated in the URL, and if no port number is specified, the default port, 80, is used.
