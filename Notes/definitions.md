# Definitions

* Code Segment
    * The location in a file where there is executable instructions
* Collabaration
    * Working with others to complete a project
* Comments
    * Text alongside code explaining the code
* Debugging
    * Removing errors from a program
* Event-driven Programming
    * A programming style where the movement of the program is determined by events happening
* Incremental Development Process
    * Approaching development small chunks at a time
* Iterative Development Process
    * Approaching development in repetitive cycles of checking and working
* Logic Error
    * When a program works but not in the right way
* Overflow Error
    * When too much data is being used and the program breaks
* Program
    * A executable set of instructions
* Program Behavior
    * How the program acts in given situations
* Program Input
    * What goes into the program
* Program Output
    * What the program spits out
* Prototype
    * A draft for a design
* Requirements
    * What is required for the program or design to be able to do
* Runtime Error
    * An error where your code cannot be executed
* Syntax Error
    * An error where you misuse syntax and your code is nonsense
* Testing
    * Making sure that code produces the desired outcome
* User Interface
    * What is interacted with by the user


# Errors
* syntax error (no colon at the end of the first line)
* syntax again (no colon)
* runtime error (divide by zero)
