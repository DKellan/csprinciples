## Chapter 7 Notes
### Repeating Steps


**For Loops: Definition**

A for loop is a simple python function allowing you to repeat a step either in a range or itterating over a list. This makes for loops great for repeating steps a fixed number of times, or for performing operations with a list.

**For Loops: Itterating Through a Range**

One of the for loops functions is itterating through a fixed range. You can do this using the range function. It works like this:

> for x in range(10):
> &emsp;print(x)

this would print this

> 0
> 1
> 2
> *and so forth*

all the way through. Notice it starts at zero that's because when you call a range it's really just repeating a list! You can also use range like this:

> for x in range(start, end+1, skip)

here's an example

> for x in range(0, 10, 2)

it would count from 1 up through 10 but not including 10. And use mutliples of 2. So you would get {0, 2, 4, 6, 8}. Neat.

**For Loops: Itterating Through a List**

The main usage of the for loop is itterating through a list or tuple. You can do this with the following syntax:

> nums = [1, 2, 4, 8, 16]
> for x in nums:
> &emsp;print(x + 1)

and you'd get:

> 2
> 3
> 5
> 9
> 17

this doesn't need to be lists of numbers though, take this for example:

> words = [butter, stock, level, three]
> for x in words:
> &emsp;print(x)

and you get:

> butter
> stock
> level
> three

**For Loops: Nested Loops**

You can nest a for loop into another foor loop too, this is most useful if you want to work with two sets at once. Take for example this code where I create partners from a list of students, they are to play chess against each other in a double round-robin style (every player plays every other player in both colors):

> white = ["john", "donald", "melissa"]
> black = ["john", "donald", "melissa"]
> for x in white:
> &emsp;for i in black:
> &emsp;&emsp;if x != i:
> &emsp;&emsp;&emsp;print(f"White: {x} - Black: {i}")
> &emsp;&emsp;else:
> &emsp;&emsp;&emsp;pass

this will return all your matchings for your double round-robin tournament:

> White: john - Black: donald
> White: john - Black: melissa
> White: donald - Black: john
> White: donald - Black: melissa
> White: melissa - Black: john
> White: melissa - Black: donald

amazing!

***Chapter 7, Closed***
