# Doctest

put this at the bottom to make doctest a thing
```
if __name__ == "__manin__":
    import doctest
    doctest.textmod()
```

put this at the beggening of a function to make it a test
```
"""
    >>>function_name(sample_item)
    desired_output
"""
``` 
