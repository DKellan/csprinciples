def square(length):
    print("*\t" * length)
    for x in range(length):
        print("\n*" + "\t" * length + "*")
    print("\n")
    print("*\t" * length)

side = int(input("Side Length:\t"))
square(side)
