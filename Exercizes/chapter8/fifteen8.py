sum = 0
count = 0
message = "Enter an integer or a negative number to stop"
value = int(input(message))
while int(value) > 0:
    print("You entered " + str(value))
    sum = sum + value
    count += 1
    value = int(input(message))
print("The sum is: " + str(sum) +
      " the average is: " + str(sum / count))