from turtle import *
jack = Screen()
jill = Turtle()

jill.penup()
jill.goto(-50, -50)
jill.pendown()

jill.setheading(180)
jill.forward(100)
for x in range(2):
	jill.right(90)
	jill.forward(100)
  
jill.penup()
jill.goto(100, 50)
jill.pendown()

jill.setheading(180)
jill.forward(100)
jill.left(90)
jill.forward(50)
jill.left(90)
jill.forward(100)
jill.right(90)
jill.forward(50)
jill.right(90)
jill.forward(100)
