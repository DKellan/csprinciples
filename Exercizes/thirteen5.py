from turtle import *
space = Screen()
bob = Turtle()
alex = Turtle()


ancalagon = Turtle()
bob.color("red")
alex.color("blue")
ancalagon.color("yellow")
bob.hideturtle()
alex.hideturtle()
ancalagon.hideturtle()

bob.penup()
bob.goto(-37.5, 0)
bob.pendown()
bob.forward(75)

alex.penup()
alex.goto(37.5, 0)
alex.setheading(90)
alex.pendown()
alex.forward(75)

ancalagon.penup()
ancalagon.goto(37.5, 0)
ancalagon.setheading(135)
ancalagon.pendown()
ancalagon.forward(75)
