from turtle import *
jack = Screen()
jill = Turtle()
bob = Turtle()
alex = Turtle()
ancalagon = Turtle()

jill.color("dark blue")
bob.color("red")
alex.color("blue")
ancalagon.color("yellow")

jill.goto(100, 0)
bob.goto(0, 100)
alex.goto(-100, 0)
ancalagon.goto(0, -100)
