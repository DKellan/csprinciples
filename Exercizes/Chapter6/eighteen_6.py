def color_square(turtle, distance, angle, *color):
  turtle.left(angle)
  for x in color:
    turtle.color(x)
    turtle.forward(distance)
    turtle.right(90)

from turtle import *
space = Screen()
joe_mama = Turtle()

color_square(joe_mama, 50, 45, "red", "dark blue", "blue", "green")
