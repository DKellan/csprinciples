def time_adder(hourstart, minutestart, hour2, minute2):
  hours = (hourstart + hour2) % 12
  hourspill = (minutestart + minute2) // 60
  donehours = (hours + hourspill) % 12
  minutes = (minutestart + minute2) % 60 
  print(f"{donehours}:{minutes:02d}")

time_adder(11, 30, 1, 35)
