def rectangle(turtle, h, w):
  turtle.setheading(90)
  for x in range(2):
    turtle.forward(h)
    turtle.right(90)
    turtle.forward(w)
    turtle.right(90)
    
from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen
malik = Turtle()        # create a turtle named malik
rectangle(malik, 10, 20)# draw a square with malik
