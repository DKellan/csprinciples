import math

def SSStriangle(turt, s1, s2, s3):
  a1p = math.acos(((s2 ** 2) + (s3 ** 2) - (s1 ** 2)) / (2 * s2 * s3))
  a2p = math.acos(((s1 ** 2) + (s3 ** 2) - (s2 ** 2)) / (2 * s1 * s3))
  a3p = math.acos(((s1 ** 2) + (s2 ** 2) - (s3 ** 2)) / (2 * s1 * s2))

  a1 = (180 * a1p) / math.pi
  a2 = (180 * a2p) / math.pi
  a3 = (180 * a3p) / math.pi

  turt.left(a2)
  turt.forward(s1)
  turt.right(180 - a3)
  turt.forward(s2)
  turt.right(180 - a1)
  turt.forward(s3)

from turtle import *
space = Screen()
turty = Turtle()

SSStriangle(turty, 48, 73, 55)
