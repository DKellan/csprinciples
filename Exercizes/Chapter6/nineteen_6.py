print("This is Madlibs!")
print("")

name = str(input("name: "))
age = str(input("age: "))
food = str(input("food: "))
event = str(input("event: "))

print("")
print(f"My name is {name} and I am {age} years old. I like to eat {food} during {event}, how about you?")
