
def product_list(*numbers):
    product = 1  # Start out with 1
    for n in numbers:
        product = product * n
    print(product)

product_list(1, 2, 3, 4, 5)