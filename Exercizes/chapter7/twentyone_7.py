def thing(arg):
    prod = 1
    sum = 0
    for x in range(2, arg, 2):
        prod = prod * x
    for i in range(1, arg, 2):
        sum = sum + i
    return (prod - sum) / ((prod + sum) / 2)

print(thing(10))
