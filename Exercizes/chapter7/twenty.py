def average(*args):
    sum = 0
    for x in args:
        sum = sum + x
    average = sum / len(args)
    return average

print(average(1, 2, 3)) 