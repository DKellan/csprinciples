# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> The program is a game in wich you: the player are being chased by robots and 
> must work to avoid them. The player can kill the robots using
> various methods.
2. Describes what functionality of the program is demonstrated in the
   video.
> It shows the player being chased by four
> robots. He demonstrates his abilities and 
> crashes the robot into each other.
3. Describes the input and output of the program demonstrated in the
   video.
> The player inputs their moves (up down,
> right, left, diagnols, teleport, etc.). And the
> game moves the robots in response.
> 

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
```
 1 from gasp import *
 2 import random
 3
 4
 5 class Player: 
 6     pass
 7
 8
 9 class Robot: 
10     pass
```
```
38     robot = Robot()
39     robot.x = random.randint(0, 63)
40     robot.y = random.randint(0, 47)
41     robot.junk = False
42     robot.shape = Box((10*robot.x, 10*robot.y), 10, 10)
```
> Here we have data stored in a data structure type i.e., an object.
> The object in question is the robots, they have attributes of x, y, and shape.
> It's good for keeping our in-game items distinct.
> 
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
```
32 def place_robots(numbots):
33 global robots
34
35 robots = []
36
37 while len(robots) < numbots:
38     robot = Robot()
39     robot.x = random.randint(0, 63)
40     robot.y = random.randint(0, 47)
41     robot.junk = False
42     robot.shape = Box((10*robot.x, 10*robot.y), 10, 10)
43     robots.append(robot)
```
> Here we use robot by creating a list filled with various 'Robot()'s.
> That way we can call from our list of 'Robot()'s
>
## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> The name of the data structure is Robot.
> 
>
2. Describes what the data contained in the list represent in your
   program
> It represents a given robot in the program.
>
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
> It helps managing complexity by adding distinction.
> Instead of the robot being a set of loose variables it
> is in the form of a nice neet compact grouping. That way you can
> create multiple without a hassle.
>
## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
```
113 def robot_crashed(the_bot):
114 for a_bot in robots:
115     if a_bot == the_bot:
116         return False
117     if a_bot.x == the_bot.x and a_bot.y == the_bot.y:
118         return a_bot 
119     return False
120
121
```
>
>
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
```
132 surviving_robots = []
133 for robot in robots:
134
135 if collided(robot, junk):
136     continue
137     jbot = robot_crashed(robot)
138
139 if not jbot:
140     surviving_robots.append(robot)
139 else: remove_from_screen(jbot.shape) jbot.junk = True
140     jbot.shape = Box(
141     (10 * jbot.x, 10 * jbot.y), 10, 10, filled=True )
142     junk.append(jbot)
```
>
>
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> The procedure allows you to check if a robot is crashed into another
> robot by checking their coordinates against one another.
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> First a function is created that takes one robot as an argument.
> The function starts by looping through the list of all robots,
> it has an if statement that checks if each specific robot is 
> the same as the argument in which case it will return False.
> It then checks to see if the argument and the bot in the loop
> are the same, if that is the case than it will return the bot in the list.
> When it does that it also returns True since the bot in the list exists
> and is therefore 'truthy'.
> If none of this proves true, it will return False.
## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
```
for robot in robots:
    if collided(robot, junk):
        continue
    jbot = robot_crashed(robot)
    if not jbot:
        surviving_robots.append(robot)
    else: 
        remove_from_screen(jbot.shape) 
        jbot.junk = True
        jbot.shape = Box((10 * jbot.x, 10 * jbot.y), 10, 10, filled=True )
        junk.append(jbot)
```
> 
>
>
> Second call:
>
>
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
> In the first call we say `jbot = robot_crashed(robot)`, thus
> we set jbot equal to both the bot thats been crashed into and 'True'.
> At this point we ask is it not true that this bot has crashed?
> if that's so then we add it to our surviving robots and on we go.
>
> Condition(s) tested by the second call:
> Here we use jbot again but with a crashed robot argument
> (i know its not exactly the question), to remove its shape from
> the screen, we set its junk attribute to true so that we know
> it shan't move. We finally replace it with a new shape,
> that of a filled box. And we append it to our list of junk.
3. Identifies the result of each call
>
> Result of the first call:
> We know that the robot isn't crashed and let it keep going.
>
>
>
> Result of the second call:
> We know that the robot is crashed and take the nescessary
> steps to record it as a junk pile that will crash with other robots.
>
>
